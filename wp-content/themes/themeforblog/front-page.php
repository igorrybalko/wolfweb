<?php get_header(); ?>
    <div class="gblock">
        <!-- wolf5 -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-9489202619963673"
             data-ad-slot="6167349579"
             data-ad-format="auto"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
     <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>
            <div class="post">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php if ( has_post_thumbnail()) { ?>
                    <div class="pre_post">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail(); ?>
                        </a>
                    </div>
                <?php } ?>
                <div class="date"><?php the_date(); ?></div>
                <?php the_content(); ?>
            </div>
    
        <?php endwhile; ?>
    <?php endif; ?>
    <div class="mywidget">
        <?php if ( is_active_sidebar( 'under_content' ) ) : ?>
            <?php dynamic_sidebar( 'under_content' ); ?>
        <?php endif; ?>
    </div>

    <?php get_sidebar(); ?>

    <?php wp_pagenavi(); ?>

<?php get_footer(); ?>