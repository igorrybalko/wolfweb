window.onload = ()=>{

    (()=>{
        let searchForm = document.getElementById('searchform'),
            inputSearch = document.getElementById('s'),
            commentForm = document.getElementById('commentform'),
            messComment = document.getElementById('comment');

        validateEmpty(searchForm, inputSearch);

        if(commentForm){
            validateEmpty(commentForm, messComment);
        }
    })();

    if(document.getElementById('clock')){
        clock();
    }  

};

(function($){
    
    $(() =>{

        $('.tabsblock').simpleTabs();

        //пример плагина
        $('.firstsel').myPlugin({
            color: 'green'
        });
        $('.secondsel').myPlugin();

        $('.accordion-block').simpleAccordion({
            cb: function () {
                console.log('cb');
            }
        });

        //dropDown menu
        (()=>{
            let itemMenu = $('ul.ddmenu_demo > li'),
                close,
                flag;

            function closeLinks(that){
                close = setTimeout(function(){
                    $(that).find('ul').stop().fadeOut();
                }, 200);
                flag = that;
            }

            itemMenu.on('mouseenter', function(){
                if(flag === this){
                    clearTimeout(close);
                }
                $(this).find('ul').stop().fadeIn();
            });

            itemMenu.on('mouseleave', function(){
                var self = this;
                closeLinks(self);
            });
        })();

        $('#test_ajax_form').on('submit', function(e){
            e.preventDefault();
            let data = $(this).serialize(),
                url = $(this).attr('action');
            $.ajax({
                method: "POST",
                url: url,
                data: data
            }).done(function(){
                alert('Данные успешно отправленны!');
            }).fail(function(){
                alert('Произошла ошибка!');
            });

        });



        $(window).on('scroll', function() {

            if($(this).scrollTop() > 100) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }

        });

        $('#toTop').on('click', function() {

            $('body,html').animate({scrollTop:0},800);

        });



    });
    
})(jQuery);