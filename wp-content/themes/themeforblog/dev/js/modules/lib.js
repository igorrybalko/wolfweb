var validateEmpty = function (form, field) {
    var getValueField = function () {
        var valueField = field.value.trim();
        if (!valueField || valueField.length < 3) {
            return false;
        }
        return true;
    };

    form.addEventListener('submit', function (event) {
        if (!getValueField()) {
            event.preventDefault();
        }
    });

};

function clock() {
    var date = new Date(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();
    if (hours < 10)
        hours = '0' + hours;
    if (minutes < 10)
        minutes = '0' + minutes;
    if (seconds < 10)
        seconds = '0' + seconds;
    var str = hours + ':' + minutes + ':' + seconds;
    document.getElementById('clock').innerHTML = str;
    setTimeout(function () {
        clock();
    }, 1000);
}
let ajaxGet = (url, callback) => {
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                alert(xhr.responseText); //текст ответа
                callback();
            } else {
                //если ошибка
                alert(xhr.statusText);
            }
        }
    };
    xhr.open('GET', url, true);
    xhr.send(null);
};
let ajaxPost = (options) => {

    /* пример параметров
     * options = {
     *      data: 'a=1&b=word&c=name',
     *       url: 'test.php',
     *       cb: function(){
     *          alert('test');
     *       }
     *  }
     */
    let sendstr = '',
        xhr = new XMLHttpRequest();

    if (typeof options.data == 'object') {
        let lastEl = Object.keys(options.data).length - 1;
        let count = 0;
        for (var k in options.data) {
            if (options.data.hasOwnProperty(k)) {
                sendstr += k + '=' + options.data[k] + (count != lastEl ? '&' : '');
                count++;
            }
        }
    } else {
        sendstr = options.data;
    }
    xhr.open('POST', options.url, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(sendstr);
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                alert(xhr.responseText); //текст ответа
                if (options.cb) {
                    options.cb();
                }
            } else {
                //если ошибка
                alert(xhr.statusText);
            }
        }
    };
};

(($)=> {
    $.fn.myPlugin = function (options) {//пример плагина
        var settings = $.extend({ //задаем параметры по умолчанию
            color: 'red'
        }, options);

        //описуем логику нашего плагина
        $(this).on('click', function () {

            var par = '<p class="par">Добавленный параграф</p>';
            $(this).after(par);
            $(this).next('.par').css('color', settings.color);

        });

        return this; //для цепочек вызовов
    }

    //плагин табов
    $.fn.simpleTabs = function (options) {

        var settings = $.extend({
            'title': '.nametab',
            'content': '.contenttab',
            'cb': ''
        }, options);

        var nametab = $(this).find(settings.title),
            contenttab = $(this).find(settings.content),
            tabsBlock = this;
        nametab.on('click', function () {
            var activeClass = $(this).hasClass('actheadtab');
            if (!activeClass) {
                var ind = $(this).index();
                $(tabsBlock).find('.actheadtab').removeClass('actheadtab');
                $(this).addClass('actheadtab');
                $(tabsBlock).find('.activetab').removeClass('activetab');
                contenttab.eq(ind).addClass('activetab');
                if (settings.cb) {
                    settings.cb();
                }
            }
        });
    };

    //аккордеон
    $.fn.simpleAccordion = function (options) {

        var settings = $.extend({
            'title': '.title-acc',
            'content': '.content-acc',
            'cb': '',
            'speed': 400
        }, options);

        var acctitle = $(this).find(settings.title);

        acctitle.on('click', function () {
            if (!$(this).next().is(':visible')) {
                $(settings.content).slideUp(settings.speed);
                $(settings.title).removeClass('active');
            }
            $(this).next().stop().slideToggle(settings.speed);
            $(this).toggleClass('active');
            if (settings.cb) {
                settings.cb();
            }
        });
    };
})(jQuery)