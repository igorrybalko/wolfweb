    </main>
</div><?php //end wrap ?>
<footer class="footer">
    <div class="wrapper">
    	<div class="bottom_top">
        	<div class="slinks">
                <div class="slinks__item"><a rel="nofollow" class="icon-github" href="https://github.com/igorrybalko/" target="_blank"></a></div>
                <div class="slinks__item"><a rel="nofollow" class="icon-bitbucket" href="https://bitbucket.org/igorrybalko/" target="_blank"></a></div>
                <div class="slinks__item"><a rel="nofollow" class="icon-linkedin" href="https://www.linkedin.com/in/igorrybalko/" target="_blank"></a></div>
                <div class="slinks__item"><a rel="nofollow" class="icon-joomla" href="https://extensions.joomla.org/profile/profile/details/453040/" target="_blank"></a></div>
            </div>

        </div>
        <div class="statistic">

                <!--LiveInternet counter--><script type="text/javascript"><!--
                document.write("<a href='//www.liveinternet.ru/click' "+
                "target=_blank><img src='//counter.yadro.ru/hit?t21.4;r"+
                escape(document.referrer)+((typeof(screen)=="undefined")?"":
                ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                ";"+Math.random()+
                "' alt='' title='LiveInternet: показано число просмотров за 24"+
                " часа, посетителей за 24 часа и за сегодня' "+
                "border='0' width='88' height='31'><\/a>")
                </script><!--/LiveInternet-->

        </div>
        <div class="copyright">
            Copyright <?php bloginfo('name'); ?> &copy; <?php echo date("Y"); ?>
        </div>
    </div>
</footer>
<div id="toTop">^</div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<?php wp_footer(); ?>
</body>
</html>