<?php get_header(); ?>
                                   
     <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>
            <div class="post">
                <h1><?php the_title(); ?></h1>
                <div class="date"><?php the_date(); ?></div>
                <div class="gblock">
                    <!-- wolf5 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9489202619963673"
                         data-ad-slot="6167349579"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <?php the_content(); ?>
                <div class="share42init" data-url="<?php the_permalink() ?>" data-title="<?php the_title() ?>"></div>
                <div class="gblock">
                    <!-- Wolf bottom -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9489202619963673"
                         data-ad-slot="7788881997"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <?php $categories = get_the_category($post->ID);

                $show_similar = $categories[0];
                if($show_similar->category_count > 2): ?>
                    <div class="similar_records">

                        <h3>Похожие записи:</h3>

                        <?php 
                        if ($categories) {
                        $category_ids = array();
                        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
                        $args=array(
                        'category__in' => $category_ids, // Сортировка производится по категориям
                        'post__not_in' => array($post->ID),
                        'showposts'=>3, //Количество выводимых записей
                        'ignore_sticky_posts'=>1); // Запрещаем повторение ссылок
                        $my_query = new wp_query($args);
                        if( $my_query->have_posts() ) {
                        echo '<ul>';
                            while ($my_query->have_posts()) {
                                $my_query->the_post();
                            ?>
                                <li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
                            <?php
                            }
                            echo '</ul>';
                        }
                        wp_reset_query();
                        } ?>
                    </div>
            	<?php endif; ?>
                <?php if ( comments_open() || get_comments_number() ) :
        comments_template();
    endif; ?>

            </div>
    
        <?php endwhile; ?>
    <?php endif; ?>
    
    <?php get_sidebar(); ?>

<?php get_footer(); ?>