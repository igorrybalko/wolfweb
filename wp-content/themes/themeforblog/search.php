<?php get_header(); ?>

<h2>По запросу " <?php the_search_query() ?> "</h2>

     <?php if(have_posts()){ ?>
        <p>найдено в следующих записях:</p>
        <ol>
            <?php while(have_posts()): the_post(); ?>

                <li>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                </li>

            <?php endwhile; ?>
        </ol>
    <?php } else{?>
        <p>Ничего не найдено :(</p>
    <?php } ?>
    
    <?php get_sidebar(); ?>
        
<?php get_footer(); ?>