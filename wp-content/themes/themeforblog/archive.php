<?php get_header(); ?>

     <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>
            <div class="post">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php if ( has_post_thumbnail()) { ?>
                    <div class="pre_post">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail(); ?>
                        </a>
                    </div>
                <?php } ?>
                <div class="date"><?php the_date(); ?></div>
                <?php the_content(); ?>

            </div>
    
        <?php endwhile; ?>
    <?php endif; ?>
    
    <?php get_sidebar(); ?>

<?php get_footer(); ?>