<!DOCTYPE html>
<html lang="ru-RU" class="no-js" >
<!-- start -->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/anariel-lite/images/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/anariel-lite/images/favicons/favicon-32x32.png">
    <link rel="shortcut icon" href="/wp-content/themes/anariel-lite/images/favicons/favicon.ico">
    <script src="/wp-content/themes/anariel-lite/js/app.min.js"></script>
    <?php wp_head();?>
    <meta name="yandex-verification" content="b10e154c8c72f0d7" />
    <meta name="google-site-verification" content="m523Ne_1y0QQWWQ5b47uQRcXNKzMkvJ7b8WlHc6OEqs" />

</head>
<!-- start body -->
<body <?php body_class(); ?> >
<!-- start header -->

<?php
?>
<header>
    <div id="headerwrap">

        <div id="header">
            <div class="header-image">
                <?php if(anariel_data('logo_position') == 1 ){
                    anariel_logo();
                } ?>
            </div>
            <!-- main menu -->
            <div class="pagenav <?php if( anariel_data('logo_position') == 3  ){ echo 'logo-left-menu'; } ?>">
                <?php if( anariel_data('logo_position') == 3  ){
                    anariel_logo();
                } ?>
                <div class="pmc-main-menu">
                    <?php
                    if ( has_nav_menu( 'anariel_mainmenu' ) ) {
                        wp_nav_menu( array(
                            'container' =>false,
                            'container_class' => 'menu-header home',
                            'menu_id' => 'menu-main-menu-container',
                            'theme_location' => 'anariel_mainmenu',
                            'echo' => true,
                            'fallback_cb' => 'anariel_fallback_menu',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'depth' => 0,
                            'walker' => new anariel_Walker_Main_Menu()));
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="respMenu">
        <div class="resp_menu_button"><i class="fa fa-list-ul fa-2x"></i></div>
    </div>
</header>
<?php
if(function_exists( 'putRevSlider')){
    if(anariel_globals('use_rev_slider_home') && is_front_page() ){ ?>
        <div id="anariel-slider-wrapper">
            <div id="anariel-slider">
                <?php putRevSlider(anariel_data('rev_slider'),"homepage") ?>
            </div>
        </div>
    <?php } ?>
<?php } ?>
<?php
if(is_front_page() && anariel_globals('use_categories')){ ?>
    <?php anariel_block_one(); ?>
<?php } ?>
<?php if(is_front_page() && anariel_globals('use_block2') ){ ?>
    <?php anariel_block_two(); ?>
<?php } ?>
<?php if(is_front_page()){ ?>
    <?php anariel_custom_layout(); ?>
<?php } ?>
