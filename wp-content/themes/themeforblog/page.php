<?php get_header(); ?>

     <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>
            <div class="post">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <div class="date"><?php the_date(); ?></div>
                <div class="gblock">
                    <!-- wolf5 -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9489202619963673"
                         data-ad-slot="6167349579"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <?php the_content(); ?>
                <div class="share42init" data-url="<?php the_permalink() ?>" data-title="<?php the_title() ?>"></div>
                <div class="gblock">
                    <!-- Wolf bottom -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9489202619963673"
                         data-ad-slot="7788881997"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
    
        <?php endwhile; ?>
    <?php endif; ?>

    <?php get_sidebar(); ?>
        
<?php get_footer(); ?>