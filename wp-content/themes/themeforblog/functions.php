<?php
function true_register_wp_sidebars() {
 
	/* В боковой колонке */
	register_sidebar(
		array(
			'id' => 'true_side', // уникальный id
			'name' => 'Боковая колонка', // название сайдбара
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.', // описание
			'before_widget' => '<div id="%1$s" class="side widget %2$s">', // по умолчанию виджеты выводятся <li>-списком
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">', // по умолчанию заголовки виджетов в <h2>
			'after_title' => '</h3>'
		)
	);
 
	/* В подвале 1*/
	register_sidebar(
		array(
			'id' => 'mod_foot1',
			'name' => 'Мод футер 1',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в футер.',
			'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);

	/* В подвале 2*/
	register_sidebar(
		array(
			'id' => 'mod_foot2',
			'name' => 'Мод футер 2',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в футер.',
			'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);

	/* В подвале 3*/
	register_sidebar(
		array(
			'id' => 'mod_foot3',
			'name' => 'Мод футер 3',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в футер.',
			'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'id' => 'under_content',
			'name' => 'Под контентом',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их.',
			'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'id' => 'search_mod',
			'name' => 'Поиск',
			'description' => 'Позиция для поиска.',
			'before_widget' => '<div id="%1$s" class="search_pos widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="hidden">',
			'after_title' => '</div>'
		)
	);
}
 
add_action( 'widgets_init', 'true_register_wp_sidebars' );

//show_admin_bar( false );

function theme_register_nav_menu() {
	register_nav_menu( 'primary', 'Top Menu' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu' );

function register_theme_styles() {

	wp_register_style( 'main', get_template_directory_uri() . '/css/style.css' );
	wp_enqueue_style( 'main' );

}
add_action( 'wp_enqueue_scripts', 'register_theme_styles' );

function register_theme_scripts() {

	wp_enqueue_script("jquery");
	wp_enqueue_script('action', get_template_directory_uri() . '/js/app.min.js');
}
add_action( 'wp_enqueue_scripts', 'register_theme_scripts' );


/*откл форматирование*/
remove_filter( 'the_content', 'wptexturize' );
remove_filter( 'the_excerpt', 'wptexturize' );
remove_filter( 'comment_text', 'wptexturize' ); 
remove_filter( 'the_title', 'wptexturize' ); 

remove_filter( 'the_content', 'wpautop' ); 
remove_filter( 'the_excerpt', 'wpautop' ); 
remove_filter( 'comment_text', 'wpautop' ); 
remove_filter( 'the_title', 'wpautop' ); 

add_theme_support( 'post-thumbnails' );

function wp_remove_version() {
    return '';
}
add_filter('the_generator', 'wp_remove_version');

//function custom_cat() {
//    // create a new taxonomy
//    register_taxonomy(
//        'timeline',
//        'timeline-item',
//        array(
//            'label' => __( 'TL Category' ),
//            'rewrite' => array( 'slug' => 'timeline', 'with_front' => false ),
//            'capabilities' => array(
//                'manage_terms' => 'manage_categories',
//                'edit_terms' => 'manage_categories',
//                'delete_terms' => 'manage_categories',
//                'assign_terms' => 'edit_posts',
//            ),
//            'show_ui' => true,
//            'show_tagcloud' => false,
//            'hierarchical' => true
//        )
//    );
//}
//add_action( 'init', 'custom_cat' );
//
//function create_post_type() {
//
//    register_post_type( 'timeline-item',
//        array(
//            'labels' => array(
//                'name' => __( 'Timeline items' ),
//                'singular_name' => __( 'Timeline items' ),
//            ),
//            'rewrite' => array( 'slug' => 'timeline/%timeline%', 'with_front' => false ),
//            'public' => true,
//            'taxonomies'  => array( 'timeline' ),
//            'hierarchical' => true,
//            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' )
//        )
//    );
//    flush_rewrite_rules();
//}
//add_action( 'init', 'create_post_type' );
//
////fix sef
//function wpa_show_permalinks( $post_link, $post ){
//    if ( is_object( $post ) && $post->post_type == 'timeline-item' ){
//        $terms = wp_get_object_terms( $post->ID, 'timeline' );
//        if( $terms ){
//            return str_replace( '%timeline%' , $terms[0]->slug , $post_link );
//        }
//    }
//    return $post_link;
//}
//add_filter( 'post_type_link', 'wpa_show_permalinks', 1, 2 );