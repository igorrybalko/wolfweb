import jQuery from 'jquery';
import AppHelper from './AppHelper';
import JqPlugins from './JqPlugins';
import Theme from './Theme';

let jqPlugins = new JqPlugins();
let appHelper = new AppHelper;
let theme = new Theme;

window.jQuery = jQuery;
window.$ = jQuery;

export default class App{

    constructor(){

        ($ => {

            document.addEventListener("DOMContentLoaded", e => {

                if(document.getElementById('clock')){
                    appHelper.clock();
                }

                $('.tabsblock').simpleTabs();

                //пример плагина
                $('.firstsel').myPlugin({
                    color: 'green'
                });
                $('.secondsel').myPlugin();

                $('.accordion-block').simpleAccordion({
                    cb: function () {
                        console.log('cb');
                    }
                });

                //dropDown menu
                (()=>{
                    let itemMenu = $('ul.ddmenu_demo > li'),
                        close,
                        flag;

                    function closeLinks(that){
                        close = setTimeout(function(){
                            $(that).find('ul').stop().fadeOut();
                        }, 200);
                        flag = that;
                    }

                    itemMenu.on('mouseenter', function(){
                        if(flag === this){
                            clearTimeout(close);
                        }
                        $(this).find('ul').stop().fadeIn();
                    });

                    itemMenu.on('mouseleave', function(){
                        var self = this;
                        closeLinks(self);
                    });
                })();

                $('#test_ajax_form').on('submit', function(e){
                    e.preventDefault();
                    let data = $(this).serialize(),
                        url = $(this).attr('action');
                    $.ajax({
                        method: "POST",
                        url: url,
                        data: data
                    }).done(function(){
                        alert('Данные успешно отправленны!');
                    }).fail(function(){
                        alert('Произошла ошибка!');
                    });

                });



                $(window).on('scroll', function() {

                    if($(this).scrollTop() > 100) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }

                });

                $('#toTop').on('click', function() {

                    $('body,html').animate({scrollTop:0},800);

                });

                document.querySelector('.respMenu').addEventListener('click', function () {

                    document.body.classList.toggle("openMenu");

                });

            });



        })(jQuery);
    }
}