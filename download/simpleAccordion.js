/*
* developed by http://wolfweb.com.ua
*/
(function($){

    $.fn.simpleAccordion = function(options){

        var settings = $.extend({
            'title' : '.title-acc',
            'content': '.content-acc',
            'cb': '',
            'speed': 400
        }, options);

        var acctitle = $(this).find(settings.title);
        
        acctitle.on('click', function(){
            if(!$(this).next().is(':visible')) {
                $(settings.content).slideUp(settings.speed);
                $(settings.title).removeClass('active');
            }
            $(this).next().stop().slideToggle(settings.speed);
            $(this).toggleClass('active');
            if(settings.cb){
                settings.cb();
            }
        });
    };

})(jQuery);